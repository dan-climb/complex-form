import "@fontsource/karla/400.css";

import React from "react";
import { AppProps } from "next/app";
import Head from "next/head";
import styled, { createGlobalStyle } from "styled-components";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Complex Form State</title>
      </Head>
      <div>
        <Global />
        <Main>
          <Component {...pageProps} />
        </Main>
      </div>
    </>
  );
}

const Global = createGlobalStyle`
    * {
      font-family: 'Karla', Arial, Helvetica, sans-serif;
    }

    body {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        color: #3B444A;
    }

    form > label {
      display: block;
      margin-top: 1rem;
    }
`;

const Main = styled.main`
  padding: 3rem;
  margin: 0 auto;
  max-width: 800px;
  width: 100%;
`;
