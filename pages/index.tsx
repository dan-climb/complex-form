import {
  FormProvider,
  useController,
  useForm,
  useFormContext,
  useWatch,
} from "react-hook-form";
import { Button } from "../components/button";
import { Input } from "../components/input";
import { RadioButtonGroup } from "../components/radioButtonGroup";
import { Select } from "../components/select";
import { callAll } from "../components/utils";

interface FormValues {
  firstName: string;
  lastName: string;
  make: { name: string; value: string } | null;
  model: { name: string; value: string } | null;
  wheels: number;
  spares: number;
}

// Derived state using useWatch - Listens for value changes to re-render
const Name = () => {
  const [firstName, lastName] = useWatch({
    name: ["firstName", "lastName"],
  });

  return (
    <h1>
      {firstName} {lastName}
    </h1>
  );
};

// Uncontrolled inputs with register from useForm
const FormInput = ({ name, ...props }: any) => {
  const { register } = useFormContext();

  return <Input {...props} {...register(name)} />;
};

// Controlled inputs with useController
const FormSelect = ({ name, ...props }: any) => {
  const {
    field: { ref, ...fieldRest },
  } = useController({ name });

  return <Select {...props} {...fieldRest} />;
};

// Controlled input with custom onChange - (All inputs should allow the custom onChange)
const FormRadioButtonGroup = ({ name, onChange, ...props }: any) => {
  const { field } = useController({ name });
  const { onChange: fieldOnChange, ref, ...inputProps } = field;
  const handleChange = callAll(fieldOnChange, onChange);

  return (
    <RadioButtonGroup {...props} {...inputProps} onChange={handleChange} />
  );
};

// Wrapper component to derive available options
const ModelSelect = () => {
  const make = useWatch({ name: "make" });

  if (!make) return null;

  const options = models.filter((model) => model.make === make.value);

  return (
    <>
      <label>Car model:</label>
      <FormSelect name="model" options={options} />
    </>
  );
};

export default function Home() {
  const methods = useForm({
    defaultValues: { firstName: "", lastName: "", wheels: 4, spares: 4 },
  });

  const { register, handleSubmit, setValue } = methods;

  const onSubmit = (data: FormValues) => console.log(data);

  return (
    <FormProvider {...methods}>
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* Inline register from useForm */}
        <label>First name:</label>
        <Input {...register("firstName")} />

        {/* Custom form input using register from context */}
        <label>Last name:</label>
        <FormInput name="lastName" />

        {/* Controlled input with useController */}
        <FormSelect name="make" label="Car make:" options={makes} />

        {/* Wrapper component for derived options */}
        <ModelSelect />

        {/* Custom onChange to update other field values */}
        <label>Number of wheels:</label>
        <FormRadioButtonGroup
          name="wheels"
          options={wheelOptions}
          onChange={(value: number) => {
            // Can also use getValues to pull out values from other fields to update state
            // or form state like isDirty for the input to conditionally update
            setValue("spares", value);
          }}
        />

        {/* Dummy input which is updated from another field */}
        <label>Number of spares:</label>
        <FormRadioButtonGroup name="spares" options={wheelOptions} />

        <Button>Submit</Button>

        {/* Derived state with useWatch */}
        <Name />
      </form>
    </FormProvider>
  );
}

const cars = [
  {
    make: "Honda",
    model: "Civic",
  },
  {
    make: "Honda",
    model: "Integra",
  },
  {
    make: "Honda",
    model: "S2000",
  },
  {
    make: "Nissan",
    model: "GTR",
  },
  {
    make: "Nissan",
    model: "Skyline",
  },
  {
    make: "Nissan",
    model: "350z",
  },
  {
    make: "Mazda",
    model: "MX-5",
  },
  {
    make: "Mazda",
    model: "RX-7",
  },
  {
    make: "Toyota",
    model: "Supra",
  },
  {
    make: "Toyota",
    model: "GT-86",
  },
];

const makes = [...new Set(cars.map(({ make }) => make))].map((make) => ({
  name: make,
  value: make,
}));

const models = cars.map(({ make, model }) => ({
  name: model,
  value: model,
  make,
}));

const wheelOptions = [
  { name: "One", value: 1 },
  { name: "Two", value: 2 },
  { name: "Three", value: 3 },
  { name: "Four", value: 4 },
];
