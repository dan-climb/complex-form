import React from "react";
import styled from "styled-components";

export interface RadioButtonProps
  extends React.InputHTMLAttributes<HTMLInputElement> {
  onChange: () => void;
}

export const RadioButton = ({
  name,
  checked,
  value,
  onChange,
}: RadioButtonProps) => {
  return (
    <Container checked={checked}>
      <Button onClick={onChange}>
        <Radio value={value} checked={checked} readOnly />
        {name}
      </Button>
    </Container>
  );
};

const Button = styled.label`
  display: block;
  padding: 8px 4px;
  transition: background-color 0.23s ease-in-out;
`;

const Container = styled.div<{ checked: boolean | undefined }>`
  background-color: ${(p) => (p.checked ? "#E5F4F6" : "#FFFFFF")};
  border: 2px solid ${(p) => (p.checked ? "#0096A9" : "#989da0")};
  border-radius: 5px;
  text-align: center;
  overflow: hidden;

  ${Button} {
    cursor: pointer;

    :hover {
      background-color: #e5f4f6;
    }
  }
`;

const Radio = styled.input.attrs({ type: "radio " })`
  display: none;
`;
