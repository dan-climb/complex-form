import styled from "styled-components";

export const Button = styled.button<{ secondary?: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 12px 24px;
  margin: 1rem 0;
  width: fit-content;
  color: ${({ secondary }) => (secondary ? "#3B444A" : "#fff")};
  background-color: ${({ secondary }) => (secondary ? "#fff" : "#4cb25f")};
  border: ${({ secondary }) => (secondary ? "2px solid #3B444A" : "none")};
  text-align: center;
  text-transform: uppercase;
  transition: 0.23s ease-in-out;
  cursor: pointer;

  :hover:enabled {
    color: #fff;
    background-color: ${({ secondary }) => (secondary ? "#3B444A" : "#3d904c")};
  }

  :disabled {
    opacity: 0.6;
    cursor: not-allowed;
  }
`;
