import { useSelect } from "downshift";
import React from "react";
import { BsChevronDown } from "react-icons/bs";
import styled from "styled-components";

interface SelectItem {
  name: React.ReactNode;
  value: any;
  option?: React.ReactNode;
  is_new_option?: string;
  recommended?: boolean;
}

export interface SelectProps
  extends Omit<React.SelectHTMLAttributes<HTMLSelectElement>, "onChange"> {
  options: SelectItem[];
  value: any;
  onChange: (value: any) => void;
  label?: string;
  name?: string;
  loading?: boolean;
  locked?: boolean;
  lockedTooltip?: JSX.Element[] | JSX.Element | string;
}

export const Select = ({
  label,
  options,
  value,
  onChange,
  loading,
  locked,
  ...props
}: SelectProps) => {
  const {
    isOpen,
    selectedItem,
    getToggleButtonProps,
    getLabelProps,
    getMenuProps,
    highlightedIndex,
    getItemProps,
  } = useSelect({
    items: options,
    selectedItem: value,
    onSelectedItemChange: ({ selectedItem }) => onChange(selectedItem),
  });

  const readOnly =
    options?.length === 1 && selectedItem?.value === options?.[0].value;

  return (
    <Container>
      {label ? <label {...getLabelProps()}>{label}</label> : null}
      <Dropdown
        open={isOpen}
        readOnly={readOnly || locked}
        loading={loading}
        {...getToggleButtonProps()}
      >
        {selectedItem ? (
          <Between>
            <p>{selectedItem.name}</p> <Tags {...selectedItem} />
          </Between>
        ) : props.placeholder ? (
          <Placeholder>{props.placeholder}</Placeholder>
        ) : props?.name ? (
          <Placeholder>Choose {props.name}...</Placeholder>
        ) : (
          options?.[0]?.name
        )}
        <DropdownIcon />
      </Dropdown>
      <SelectMenu {...getMenuProps()}>
        {isOpen &&
          options?.map((item, index) => {
            const { name, value, option, ...tags } = item;
            return (
              <SelectItem
                key={value}
                data-value={value}
                highlighted={highlightedIndex === index}
                selected={selectedItem?.value === value}
                {...getItemProps({ item, index })}
              >
                {option ?? name} <Tags {...tags} />
              </SelectItem>
            );
          })}
      </SelectMenu>
    </Container>
  );
};

interface TagsProps {
  is_new_option?: string;
  recommended?: boolean;
  canvaIncompatible?: boolean;
}

export const Tags = ({
  is_new_option,
  recommended,
  canvaIncompatible,
}: TagsProps) => {
  return (!is_new_option || is_new_option === "0") &&
    !recommended &&
    !canvaIncompatible ? null : (
    <TagsContainer>
      {is_new_option === "1" ? <Tag newProduct>n</Tag> : null}
      {recommended ? <Tag>r</Tag> : null}
      {canvaIncompatible ? <CanvaStrike /> : null}
    </TagsContainer>
  );
};

const Container = styled.div`
  position: relative;
  & > label {
    display: block;
    margin-top: 1rem;
  }
`;

export const DropdownIcon = styled(BsChevronDown)`
  position: absolute;
  right: 4px;
  top: 4px;
  height: calc(100% - 8px);
  padding: 4px;
  color: #fff;
  background-color: #0096a9;
  font-size: 40px;
  border-radius: 4px;
  box-sizing: border-box;
`;

interface DropdownProps {
  open: boolean;
  loading?: boolean;
  readOnly?: boolean;
  locked?: boolean;
}

export const Dropdown = styled.div<DropdownProps>`
  display: inline-block;
  position: relative;
  padding: 16px;
  width: 100%;
  height: 50px;
  border-radius: 5px;
  border: ${({ readOnly }) =>
    readOnly ? "1px solid #EBECEC" : "1px solid #989da0"};
  background-color: ${({ readOnly, loading }) =>
    readOnly ? "#EBECEC" : loading ? "#989da0" : "#FFF"};
  cursor: ${({ readOnly }) => (readOnly ? "auto" : "pointer")};
  opacity: ${({ loading }) => (loading ? 0.3 : 1)};
  box-sizing: border-box;

  ${DropdownIcon} {
    display: ${({ readOnly }) => (readOnly ? "none" : "block")};
    transform: ${({ open }) => `rotateZ(${open ? "180deg" : 0})`};
  }

  ${({ open }) =>
    open
      ? `
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    `
      : ""}
`;

export const SelectMenu = styled.ul<{ open: boolean }>`
  display: block;
  position: absolute;
  left: 0;
  top: 100%;
  outline: 0;
  margin: 0;
  padding: 0;
  min-width: 100%;
  width: 100%;
  max-height: 286px;
  background-color: #5b6771;
  color: #fff;
  border-radius: 0 0 5px 5px;
  overflow-x: hidden;
  overflow-y: auto;
  z-index: 1;

  &::-webkit-scrollbar {
    -webkit-appearance: none;

    :vertical {
      width: 11px;
    }
  }

  &::-webkit-scrollbar-thumb {
    border-radius: 8px;
    border: 2px solid #5b6771;
    background-color: rgba(255, 255, 255, 0.5);
  }
`;

const getItemBackground = ({ incompatible, highlighted, selected }: any) => {
  if (incompatible) return "inherit";
  if (highlighted) return "#e75300";
  if (selected) return "#3B444A";
  return "inherit";
};

export const SelectItem = styled.li<{
  incompatiable?: boolean;
  selected?: boolean;
  highlighted?: boolean;
}>`
  display: flex;
  justify-content: space-between;
  padding: 12px 16px;
  background-color: ${getItemBackground};
  white-space: normal;
  word-wrap: normal;
  cursor: ${({ incompatiable }) => (incompatiable ? "not-allowed" : "pointer")};
  transition: background-color 0.23s ease-in-out;

  p {
    margin: 0;
    text-decoration: ${({ incompatiable }) =>
      incompatiable ? "line-through" : "none"};
    opacity: ${({ incompatiable }) => (incompatiable ? 0.5 : 1)};
  }

  span {
    white-space: nowrap;
  }
`;

const Between = styled.div`
  display: flex;
  justify-content: space-between;
  margin-right: 40px;

  p {
    margin: 0;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  span {
    position: relative;
    top: -2px;
    white-space: nowrap;
  }
`;

export const Placeholder = styled.span`
  color: #989da0;
`;

const TagsContainer = styled.span`
  display: inline-flex;
  align-items: center;

  svg {
    margin-left: 12px;
  }
`;

export const Tag = styled.div<{ newProduct?: boolean }>`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  border-radius: 100%;
  padding: 4px;
  margin-left: 12px;
  font-size: 12px;
  width: 12px;
  height: 12px;
  color: #fff;
  background: ${({ newProduct }) => (newProduct ? "#1BBAA8" : "#989DA0")};
  font-weight: 300;
  text-transform: uppercase;
  box-sizing: content-box;
`;

const CanvaStrike = () => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="44.5"
      height="15.581"
      viewBox="0 0 44.5 15.581"
    >
      <g transform="translate(0 0)">
        <g>
          <path
            d="M39,47.452a5.128,5.128,0,0,1-3.126,1.691,1.216,1.216,0,0,1-1.281-1.179,2.219,2.219,0,0,1,.051-.82,21.983,21.983,0,0,1,.82-2.87c0-.1-.051-.154-.1-.154-.513,0-2.255,1.794-2.511,2.973l-.205.974a1.388,1.388,0,0,1-1.23,1.076.39.39,0,0,1-.41-.359v-.308l.1-.513A5.291,5.291,0,0,1,28.7,49.143a1.04,1.04,0,0,1-1.128-.871,1.955,1.955,0,0,1-1.589.871,2.1,2.1,0,0,1-1.845-1.281,9.379,9.379,0,0,1-1.845,1.589,6.876,6.876,0,0,1-3.639,1.23,4.443,4.443,0,0,1-2.973-1.23,5.314,5.314,0,0,1-1.538-3.229,11.566,11.566,0,0,1,5.176-10.4,4.871,4.871,0,0,1,2.46-.718A2.971,2.971,0,0,1,24.8,37.663a2.779,2.779,0,0,1-1.743,3.024c-.718.359-1.076.359-1.179.154a.335.335,0,0,1,.1-.41,3.024,3.024,0,0,0,1.179-3.28,1.34,1.34,0,0,0-1.23-1.333c-2.665,0-6.355,5.792-5.843,9.994.205,1.64,1.23,3.536,3.28,3.536a4.848,4.848,0,0,0,2.05-.513,6.446,6.446,0,0,0,2.409-1.794,4.242,4.242,0,0,1,4-4.408c1.076,0,1.948.41,2.05,1.281A1.069,1.069,0,0,1,28.749,45.2c-.308,0-.769-.1-.82-.359-.051-.308.666-.154.564-.82-.051-.41-.513-.564-.923-.564-1.538,0-2.409,2.153-2.255,3.485.051.615.41,1.23.923,1.23.461,0,1.128-.666,1.333-1.589A1.388,1.388,0,0,1,28.8,45.5c.256,0,.41.1.41.359v.308c-.051.256-.256,1.23-.256,1.384a.4.4,0,0,0,.41.41,4.957,4.957,0,0,0,1.9-1.179c.256-1.281.564-2.87.564-2.973a1.163,1.163,0,0,1,1.333-1.076.39.39,0,0,1,.41.359V43.4l-.256,1.384a5.14,5.14,0,0,1,3.229-2.153.676.676,0,0,1,.718.564,2.646,2.646,0,0,1-.154.974,21.7,21.7,0,0,0-.871,3.178.436.436,0,0,0,.461.564A3.667,3.667,0,0,0,39,46.324v-.513a6.658,6.658,0,0,1,.256-2.2,1.663,1.663,0,0,1,1.333-1.076.377.377,0,0,1,.41.41.65.65,0,0,1-.051.308,9.032,9.032,0,0,0-.513,2.716,8.488,8.488,0,0,0,.205,1.691.136.136,0,0,0,.154.154,6.757,6.757,0,0,0,1.9-2.2,2.361,2.361,0,0,1-1.025-2.1c0-1.538.923-2.358,1.845-2.358a1.355,1.355,0,0,1,1.333,1.538,4.893,4.893,0,0,1-.615,2.1h.205a1.564,1.564,0,0,0,1.23-.564,1.359,1.359,0,0,1,.308-.256,3.942,3.942,0,0,1,3.024-1.486c1.076,0,1.948.41,2.05,1.281a1.134,1.134,0,0,1-1.128,1.333c-.308,0-.82-.1-.82-.359-.051-.256.666-.154.615-.82-.051-.41-.513-.615-.923-.615-1.435,0-2.409,1.9-2.255,3.485.051.615.359,1.23.923,1.23.461,0,1.128-.666,1.384-1.589a1.456,1.456,0,0,1,1.23-1.076.363.363,0,0,1,.41.359,11.5,11.5,0,0,1-.256,1.435,4.415,4.415,0,0,0-.1,1.076,2.306,2.306,0,0,0,.615,1.281.468.468,0,0,1,.154.256.3.3,0,0,1-.308.308.308.308,0,0,1-.205-.051,2.506,2.506,0,0,1-1.691-1.9,1.9,1.9,0,0,1-1.589.82,2.284,2.284,0,0,1-2.1-2A3.444,3.444,0,0,1,45.15,45.5a2.346,2.346,0,0,1-1.23.41h-.359a10.26,10.26,0,0,1-2.768,2.819,2.107,2.107,0,0,1-.82.256.556.556,0,0,1-.461-.205A1.793,1.793,0,0,1,39,47.452Zm3.485-4.613a2.626,2.626,0,0,0,.666,1.64,3.523,3.523,0,0,0,.256-1.23c0-.769-.308-1.128-.513-1.128C42.587,42.122,42.485,42.532,42.485,42.839Z"
            transform="translate(-10.32 -35.1)"
            fill="#adb3b8"
            opacity="0.5"
          />
          <line
            x2="44.5"
            transform="translate(0 10.5)"
            fill="none"
            stroke="#adb3b8"
            strokeWidth="1"
          />
        </g>
      </g>
    </svg>
  );
};
