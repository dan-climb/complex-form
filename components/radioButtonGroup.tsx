import React from "react";
import styled from "styled-components";
import { RadioButton } from "./radioButton";

export interface RadioButtonGroupProps {
  options: {
    name: string;
    value: number;
  }[];
  onChange: (value: number) => void;
  value: number | null;
}

export const RadioButtonGroup = ({
  options,
  onChange,
  value: selectedValue,
}: RadioButtonGroupProps) => {
  return (
    <ButtonGroup>
      {options?.map(({ name, value, ...rest }) => (
        <RadioButton
          key={value}
          name={name}
          checked={value === selectedValue}
          onChange={() => onChange(value)}
          {...rest}
        />
      ))}
    </ButtonGroup>
  );
};

const ButtonGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 8px 16px;
  align-items: flex-end;
`;
