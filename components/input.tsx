import styled from "styled-components";

export const Input = styled.input<{ prefix?: string }>`
  display: inline-block;
  position: relative;
  padding: 16px;
  width: 100%;
  border: 1px solid #989da0;
  border-radius: 5px;
  box-sizing: border-box;

  &:read-only {
    border: 1px solid #ebecec;
    background-color: #ebecec;
    outline: none;
  }
`;
