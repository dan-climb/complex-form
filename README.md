## Complex Form

A small playground to try out different solutions for tricky form bits in trade buys primarily around dependant fields.

Uses a few of the base components from [wttb-apps](https://gitlab.com/climbcreative/wttb-apps)

---

### Current Solutions

- [react-hook-form](https://react-hook-form.com/ )
- [formik](https://formik.org/)
